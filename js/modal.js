var modal = (function(window) {
	var
		method = {},
		$overlay,
		$modal,
		$content,
		$close;

	// Center the modal in the viewport
	method.center = function () {
		var top, left;

		top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
		left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

		$modal.css({
			top: top + $(window).scrollTop(),
			left: left + $(window).scrollLeft()
		});
	};

	// Open the modal
	method.open = function (c) {
		$content.empty().append(c);
		// method.center();
		$(window).bind('resize.modal', method.center);
		$overlay.show();
		$modal.show("fast", function () {
			method.center();
		});
	};

	// Close the modal
	method.close = function () {

		$modal.hide();
		$overlay.hide();
		$content.empty();
		$(window).unbind('resize.modal');

	};

	//Initialze modal
	function init(){

		// Generate the HTML and add it to the document
		$overlay = $('<div id="overlay"></div>');
		$modal = $('#modal');
		$content = $('#content');
		$close = $('#close');

		$modal.hide();
		$overlay.hide();

		$(document).ready(function () {
			$('body').append($overlay);
		});

		$close.click(function (e) {
			e.preventDefault();
			method.close();
		});

	}

	init();

	return method;

}(window));

