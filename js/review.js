var review = (function(w) {

	return {

		sortedBy:'',

		onSort:null,

		createLi: function (obj) {

			var li = $('<li class="one_review" class="id"></li>');

			var strong = $('<strong class="review_score">'+obj.score+'</strong>');

			var blockquote = $('<blockquote class="review_content"></blockquote>');

			var cite = $('<cite>'+obj.cite+'</cite>');

			blockquote.append(obj.content,cite);

			li.append(strong,blockquote);

			return li;

		},

		orderBy:function(order) {

			var sb = review.sortedBy;

			if (sb.includes('-') && sb.includes(order)) {
				sb = sb.replace('-', '');

				$('#reviewSort').html("Order by best ratings");

			} else {
				if (sb.includes(order)) {
					sb = '-'+ order;
					$('#reviewSort').html("Order by worst ratings");
				} else {
					sb = order;
				}
			}

			review.sortedBy = sb;

			return w.reviewData.sort(dynamicSort(review.sortedBy));

		},

		populate: function (data) {

			//var sorted = review.orderBy(order);

			var ul = $(".reviews_list").empty();

			$.each(data, function (i, item) {

				ul.append(review.createLi(item));

			});

		}

	}

})(window);
