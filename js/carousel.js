var carousel = (function(w,$) {

	return {

		//Return tha path of the large photo
		getLargePhoto: function (id) {

			var large, cd = w.carouselData;

			large = cd.path + '/' + cd.photos[id].large;

			return large;

		},

		//Creates a element to append to a list
		createElement: function (id, path, desc) {

			var element;

			element = $('<li class="one_photo" id="' + id + '"><img src="' + path + '" alt="' + desc + '"/></li>').click(function (e) {

				var id = $(this).attr("id");

				modal.open('<img id="'+id+'"src="'+carousel.getLargePhoto(id)+'" alt="'+desc+'"/>');

				e.preventDefault();

			});

			return element;

		},

		//Chnange the element on a modal
		changeElement: function(id, path, desc){

			var element = $('#content img');

			element.attr('id',id);

			element.attr('src',path);

			element.attr('alt',desc);

		},

		//Get the prev photo to change the modal
		prev: function () {

			var element = $('#content img'), id =  parseInt(element.attr("id")), prev;

			var cd = w.carouselData;

			if (id==0){
				prev = cd.photos.length -1;
			}else{
				prev = id - 1;
			}

			carousel.changeElement(prev, carousel.getLargePhoto(prev), cd.photos[prev].desc);

		},

		//Get the next photo to change the modal
		next:function(){

			var element = $('#content img'), id =  parseInt(element.attr("id")), next;

			var cd = w.carouselData;

			if (id==cd.photos.length -1){
				next = 0;
			}else{
				next = id + 1;
			}

			carousel.changeElement(next, carousel.getLargePhoto(next), cd.photos[next].desc);

		},

		//Init the carousel
		init: function () {

			var thumb, cd = w.carouselData;

			for (var i = 0; i < cd.photos.length; i++) {

				thumb = cd.path + '/' + cd.photos[i].thumb;

				$("#carousel ul").append(carousel.createElement(i, thumb, cd.photos[i].desc));

			}

			$('#carouselPrev').click(function(){
				carousel.prev();
			});

			$('#carouselNext').click(function(){
				carousel.next();
			});

		}

	}

})(window, jQuery);
