var room = (function(w) {

	return {

		sortedBy:'',

		createTr: function (id,obj) {

			var tr = $('<tr class="one_room" class="id"></tr>');

			var tdname = $('<td class="room_name">'+obj.name+'</td>');

			var tdoccupancy = $('<td class="room_occupancy">'+obj.occupancy+'</td>');

			var tdprice = $('<td class="room_price">&euro;'+obj.price+'</td>');

			var tdquantity = $('<td class="room_quantity"></td>');

			if (obj.selected) {
				var tdtotal = $('<td class="room_total">&euro;' +parseFloat(parseFloat(obj.selected)*parseFloat(obj.price)).toFixed(2)+'</td>');
			}else{
				var tdtotal = $('<td class="room_total">&euro;0</td>');
			}

			var select = $('<select name="room['+obj.name.replace(/ /g,'')+']"></select>');

			for(var i=0; i<= parseInt(obj.quantity);i++) {

				if (i == 0 && !obj.selected) {
					select.append('<option value="' + i + '" selected="selected">' + i + '</option>');
				} else {
					if (i ==obj.selected) {
						select.append('<option value="' + i + '" selected="selected">' + i + '</option>');
					} else {
						select.append('<option value="' + i + '">' + i + '</option>');
					}
				}

			}

			select.change(function(){

				var tdtotal = $(this).parent().siblings('.room_total');

				tdtotal.html('&euro;'+ parseFloat(parseFloat(obj.price)*parseFloat(this.value)).toFixed(2));

				w.roomData[id].selected = this.value;

			});

			tdquantity.append(select);

			tr.append(tdname, tdoccupancy, tdprice, tdquantity, tdtotal);

			return tr;

		},

		orderBy:function(order) {

			var sb = room.sortedBy;

			if (sb.includes('-') && sb.includes(order)) {
				sb = sb.replace('-', '');
			} else {
				if (sb.includes(order)) {
					sb = '-'+ order;
				} else {
					sb = order;
				}
			}

			room.sortedBy = sb;

			return w.roomData.sort(dynamicSort(room.sortedBy));

		},

		populate: function (order) {

			var sorted = room.orderBy(order);

			var table = $(".rooms_table tbody").empty();

			$.each(sorted, function (i, item) {

				table.append(room.createTr(i,item));

			});

		}

	}

})(window);
