;(function() {

	//Initalize de app.
	function init(){

		carousel.init();

		room.populate("name");

		review.onSort = (function(sort){

			pagination.init(review.orderBy(sort));

		});

		pagination.onChangePage = function (data) {

			review.populate(data);

		};

		review.onSort("-score");

	}

	init();

})();
