var pagination = (function() {

	return {

		page:1,

		pageSize: 7,

		data:[],

		pageData:[],

		onChangePage:null,

		setPage: function(page){

			pagination.pageData=[];

			var end = Math.ceil(pagination.pageSize*page);

			var start = end - pagination.pageSize;

			if (end > pagination.data.length) {
				var auxEnd = end - pagination.data.length;
				end = pagination.data.length;
				start = end - (pagination.pageSize - auxEnd);
			}

			for(var i = start; i < end; i++) {
				pagination.pageData.push(pagination.data[i]);
			}

			pagination.page = page;

			pagination.populate(pagination.pageData);

		},

		nextPage:function(){


			if (pagination.page < pagination.calculateNumberPages()){

				pagination.setPage(parseInt(pagination.page) + 1);

			}

		},

		prevPage:function(){

			if (pagination.page > 1){

				pagination.setPage(parseInt(pagination.page) - 1);

			}

		},

		calculateNumberPages: function () {

			return Math.ceil(pagination.data.length / pagination.pageSize);

		},

		populate:function(data){

			var numberOfPages = pagination.calculateNumberPages();

			var ul = $('<ul></ul>');

			var first = $('<li id="first" class="button">first</li>').click(function(){

				pagination.setPage(1);

			});

			var prev = $('<li id="prev" class="button">prev</li>').click(function(){

				pagination.prevPage();

			});

			ul.append(first,prev);

			for(var i=1; i<=numberOfPages;i++){

				var page = $('<li id="page'+i+'" class="button">'+i+'</li>').click(function(){

					var id = $(this).attr('id');

					pagination.setPage(id.replace("page",""));

				});

				ul.append(page);

			}

			var next = $('<li id="next" class="button">next</li>').click(function(){

				pagination.nextPage();

			});

			var last = $('<li id="last" class="button">last</li>').click(function(){

				pagination.setPage(numberOfPages);

			});

			ul.append(next,last);

			$('#pagination').empty().append(ul);

			pagination.onChangePage(data);

		},

		init: function(data){

			pagination.data = data;

			pagination.setPage(1);

		}

	}

})();
